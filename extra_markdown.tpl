{% extends 'markdown.tpl' %}

{%- block data_other -%}
{%- for type in output.data | filter_data_type -%}
{%- if type == 'application/vnd.plotly.v1+json' -%}
{%- set fname = output.metadata.filenames['application/vnd.plotly.v1+json'] -%}
{%- set plotly_url = fname | path2url -%}
{%- set div_id = fname.split('/') | last -%}
<div id="{{div_id}}"><img ></img></div>

<script>
window.addEventListener('load', function() {
  Plotly.d3.json('../{{plotly_url}}', function(error, fig) {
    Plotly.plot('{{div_id}}', fig.data, fig.layout).then(
      function(value) {Plotly.relayout('{{div_id}}', {height: ' '})}
    );
  });
});
</script>
{%- endif -%}
{%- endfor -%}
{%- endblock -%}

{% block error %}
{% endblock error %}

{% block stream %}
{%- if output.name == 'stdout' -%}
{{ output.text | indent }}
{%- endif -%}
{% endblock stream %}
