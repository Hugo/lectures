```python
import numpy as np
import plotly.offline as py
import plotly.graph_objs as go

pi = np.pi
```

# Tight binding and nearly free electrons
_(based on chapters 15–16 of the book)_  
Exercises 15.1, 15.3, 15.4, 16.1, 16.2

!!! summary "Learning goals"

    After this lecture you will be able to:

    - formulate a general way of computing the electron band structure - the **Bloch theorem**.
    - recall that in a periodic potential all electron states are Bloch waves.
    - derive the electron band structure when the interaction with the lattice is weak using the **Nearly free electron model**.

Let's summarize what we learned about electrons so far:

* Free electrons form a Fermi sea ([lecture 4](4_sommerfeld_model.md))
* Isolated atoms have discrete orbitals ([lecture 5](5_atoms_and_lcao.md))
* When orbitals hybridize we get *LCAO* or *tight-binding* band structures ([lecture 7](7_tight_binding.md))

The nearly free electron model (the topic of this lecture) helps to understand the relation between tight-binding and free electron models. It describes the properties of metals.

These different models can be organized as a function of the strength of the lattice potential $V(x)$:

![](figures/models.svg)

## Bloch theorem

> All Hamiltonian eigenstates in a crystal have the form
> $$ \psi_n(\mathbf{r}) = u_n(\mathbf{r})e^{i\mathbf{kr}} $$
> with $u_n(\mathbf{r})$ having the same periodicity as the lattice potential $V(\mathbf{r})$, and index $n$ labeling electron bands with energies $E_n(\mathbf{k})$.

In other words: any electron wave function in a crystal is a product of a periodic part that describes electron motion within a unit cell and a plane wave.

### Extra remarks

The wave function $u_n(\mathbf{r})e^{i\mathbf{kr}}$ is called a **Bloch wave**.

The $u_n(\mathbf{r})$ part is some unknown function. To calculate it we need to solve the Schrödinger equation. It is hard in general, but there are two limits when $U$ is "weak" and $U$ is "large" that provide us with most intuition.

If we change $\mathbf{k}$ by a reciprocal lattice vector $\mathbf{k} \rightarrow \mathbf{k} + h\mathbf{b}_1 + k\mathbf{b}_2 + l\mathbf{b}_3$, and we change $u_n(\mathbf{r}) \rightarrow u_n(\mathbf{r})\exp\left[-h\mathbf{b}_1 - k\mathbf{b}_2 - l\mathbf{b}_3\right]$ (also periodic!), we obtain the same wave function. Therefore energies of all bands $E_n(\mathbf{k})$ are periodic in reciprocal space with the periodicity of the reciprocal lattice.

Bloch theorem is extremely similar to the ansatz we used in [1D](7_tight_binding.md), and to the description of the [X-ray scattering](10_xray.md).

## Nearly free electron model

In free electron model $E = \hbar^2 k^2/2m$.

* There is only one band
* The band structure is not periodic in $k$-space
* In other words the Brillouin zone is infinite in momentum space

Within the **nearly free electron model** we want to start from the dispersion relation of the free electrons and consider the effect of introducing a weak lattice potential. The logic is very similar to getting optical and acoustic phonon branches by changing atom masses (and therefore reducing the size of the Brillouin zone).

In our situation:

![](figures/nearly_free_electron_bands.svg)

The band gaps open where two copies of the free electron dispersion cross.

### Avoided level crossing

*Remark: this is an important concept in quantum mechanics, based on the perturbation theory. You will only learn it later in QMIII, so we will need to postulate some important facts.*

Let's focus on the first crossing. The momentum near it is $k = \pi/a + \delta k$ and we have two copies of the original band structure coming together. One with $\psi_+ \propto e^{i\pi x/a}$, another with $\psi_- \propto e^{-i\pi x/a}$. Near the crossing the wave function is the linear superposition of $\psi_+$ and $\psi_-$: $\psi = \alpha \psi_+ + \beta \psi_-$. We actually used almost the same form of the wave function in LCAO, except instead of $\psi_\pm$ we used the orbitals $\phi_1$ and $\phi_2$ there.

Without the lattice potential we can approximate the Hamiltonian of these two states as follows:
$$H\begin{pmatrix}\alpha \\ \beta \end{pmatrix} =
\begin{pmatrix} E_0 + v \hbar \delta k & 0 \\ 0 & E_0 - v \hbar \delta k\end{pmatrix}
\begin{pmatrix}\alpha \\ \beta \end{pmatrix}.
$$

Here we used $\delta p = \hbar \delta k$, and we expanded the quadratic function into a linear plus a small correction.  

??? question "calculate $E_0$ and the velocity $v$"
    The edge of the Brilloin zone has $k = \pi/a$. Substituting this in the free electron dispersion $E = \hbar^2 k^2/2m$ we get $E_0 = \hbar^2 \pi^2/2 m a^2$, and $v=\hbar p/m=\hbar \pi/ma$.

Without $V(x)$ the two wave functions $\psi_+$ and $\psi_-$ are independent since they have a different momentum. When $V(x)$ is present, it may couple these two states.

So in presence of $V(x)$ the Hamiltonian becomes

$$
H\begin{pmatrix}\alpha \\ \beta \end{pmatrix} =
\begin{pmatrix} E_0 + v \hbar \delta k & W \\ W^* & E_0 - v \hbar \delta k\end{pmatrix}
\begin{pmatrix}\alpha \\ \beta \end{pmatrix},
$$

Here the coupling strength $W = \langle \psi_+ | V(x) | \psi_- \rangle$ is the matrix element of the potential between two states. *(This where we need to apply the perturbation theory, and this is very similar to the LCAO Hamiltonian)*.

??? question "how does our solution satisfy the Bloch theorem? What is $u(x)$ in this case?"
    The wave function has a form $\psi(x) = \alpha \exp[ikx] + \beta \exp[i(k - 2\pi/a)x]$
    (here $k = \pi/a + \delta k$). Choosing $u(x) = \alpha + \beta \exp(2\pi i x/a)$ we see
    that $\psi(x) = u(x) \exp(ikx)$.

#### Dispersion relation near the avoided level crossing

We need to diagonalize a 2x2 matrix Hamiltonian. The answer is
$$ E(\delta k) = E_0 \pm \sqrt{v^2\hbar^2\delta k^2 + |W|^2}$$

Check out section 15.1.1 of the book for the details of this calculation.

#### Physical meaning of $W$

Now we expand the definition of $W$:

$$W = \langle \psi_+ | V(x) | \psi_- \rangle = \int_0^{a} dx \left[e^{i\pi x/a}\right]^* V(x) \left[e^{-i\pi x/a}\right] = \int_0^a e^{2\pi i x /a} V(x) = V_1$$

Here $V_1$ is the first Fourier component of $V(x)$ (using a complex Fourier transform).

$$ V(x) = \sum_{n=-\infty}^{\infty} V_n e^{2\pi i n x/a}$$

#### Crossings between the higher bands

Everything we did applies to the crossings at higher energies, only there we would get higher Fourier components of $V(x)$: $V_2$ for the crossing between the second and third band, $V_3$ for the crossing between third and 4th, etc.

### Repeated vs reduced vs extended Brillouin zone

All different ways to **plot** the same dispersion relation (no difference in physical information).

Repeated BZ (all possible Bloch bands):

![](figures/nearly_free_electron_bands.svg)

* Contains redundant information
* May be easier to count/follow the bands

Reduced BZ (all bands within 1st BZ):

![](figures/reduced_nearly_free_electron_bands.svg)

* No redundant information
* Hard to relate to original dispersion

Extended BZ (n-th band within n-th BZ):

![](figures/extended_nearly_free_electron_bands.svg)

* No redundant information
* Easy to relate to free electron model
* Contains discontinuities
