```python
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()
```

# Drude theory and the free electron model
_(based on chapters 3–4 of the book)_  
Exercises 3.1, 3.3, 4.2, 4.5, 4.6, 4.7

!!! summary "Learning goals"

    After this lecture you will be able to:

    - discuss the basics of 'Drude theory', which describes electron motion in metals.
    - use Drude theory to analyze transport of electrons through conductors in electric and magnetic fields.
    - describe central terms such as the mobility and the Hall resistance.

### Drude theory
Ohm's law states that $V=IR=I\rho\frac{l}{A}$. In this lecture we will investigate where this law comes from. We will use the theory developed by Paul Drude in 1900, which is based on three assumptions:

- Electrons have an average scattering time $\tau$.
- At each scattering event an electron returns to momentum ${\bf p}=0$.
- In-between scattering events electrons respond to the Lorentz force ${\bf F}_{\rm L}=-e\left({\bf E}+{\bf v}\times{\bf B}\right)$.

For now we will consider only an electric field (_i.e._ ${\bf B}=0$). What velocity do electrons acquire in-between collisions?

$$
{\bf v}=-\int_0^\tau\frac{e{\bf E}}{m_{\rm e}}{\rm d}t=-\frac{e\tau}{m_{\rm e}}{\bf E}=-\mu{\bf E}
$$

Here we have defined the quantity $\mu\equiv e\tau/m_{\rm e}$, which is the _mobility_. If we have a density $n$ of electrons in our solid, the current density ${\bf j}$ [A/m$^2$] then becomes:

$$
{\bf j}=-en{\bf v}=\frac{n e^2\tau}{m_{\rm e}}{\bf E}=\sigma{\bf E}\ ,\ \ \sigma=\frac{ne^2\tau}{m_{\rm e}}=ne\mu
$$

$\sigma$ is the conductivity, which is the inverse of resistivity: $\rho=\frac{1}{\sigma}$. If we now take $j=\frac{I}{A}$ and $E=\frac{V}{l}$, we retrieve Ohm's Law: $\frac{I}{A}=\frac{V}{\rho l}$.

Scattering is caused by collisions with:

- Phonons: $\tau_{\rm ph}(T)$ ($\tau_{\rm ph}\rightarrow\infty$ as $T\rightarrow 0$)
- Impurities/vacancies: $\tau_0$

Scattering rate $\frac{1}{\tau}$:

$$
\frac{1}{\tau}=\frac{1}{\tau_{\rm ph}(T)}+\frac{1}{\tau_0}\ \Rightarrow\ \rho=\frac{1}{\sigma}=\frac{m}{ne^2}\left( \frac{1}{\tau_{\rm ph}(T)}+\frac{1}{\tau_0} \right)\equiv \rho_{\rm ph}(T)+\rho_0
$$

![](figures/matthiessen.svg)

_Matthiessen's Rule_ (1864). Solid (dashed) curve: $\rho(T)$ for a pure (impure) crystal.

How fast do electrons travel through a copper wire? Let's take $E$ = 1 volt/m, $\tau$ ~ 25 fs (Cu, $T=$ 300 K).

$\rightarrow v=\mu E=\frac{e\tau}{m_{\rm e}}E=\frac{10^{-19}\times 2.5\times 10^{-14}}{10^{-30}}=2.5\times10^{-3}=2.5$ mm/s ! (= 50 $\mu$m @ 50 Hz AC)

### Hall effect
Consider a conductive wire in a magnetic field ${\bf B} \rightarrow$ electrons are deflected in a direction perpendicular to ${\bf B}$ and ${\bf j}$.

![](figures/hall_effect.svg)

${\bf E}_{\rm H}$ = _Hall voltage_, caused by the Lorentz force.

In equilibrium, assuming that the average velocity becomes zero after every collision: $\frac{mv_x}{\tau}=-eE$

The $y$-component of the Lorentz force $-e{\bf v}_x\times{\bf B}$ is being compensated by the Hall voltage ${\bf E}_{\rm H}={\bf v}_x\times{\bf B}=\frac{1}{ne}{\bf j}\times{\bf B}$. The total electric field then becomes

$$
{\bf E}=\left(\frac{1}{ne}{\bf j}\times{\bf B}+\frac{m}{ne^2\tau}{\bf j}\right)
$$

We now introduce the _resistivity matrix_ $\tilde{\rho}$ as ${\bf E}=\tilde{\rho}{\bf j}$, where the diagonal elements are simply $\rho_{xx}=\rho_{yy}=\rho_{zz}=\frac{m}{ne^2\tau}$. The off-diagonal element $\rho_{xy}$ gives us:

$$
\rho_{xy}=\frac{B}{ne}\equiv -R_{\rm H}B
$$

where $R_{\rm H}=-\frac{1}{ne}$ is the _Hall resistance_. So by measuring the Hall resistance, we can obtain $n$, the density of free electrons in a material.

While most materials have $R_{\rm H}>0$, interestingly some materials are found to have $R_{\rm H}<0$. This would imply that the charge carriers either have a positive charge, or a negative mass. We will see later (chapter 17) how to interpret this.
