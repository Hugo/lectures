```python
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()
```

### Sommerfeld theory (free electron model)

!!! summary "Learning goals"

    After this lecture you will be able to:

    - calculate the electron density of states in 1D, 2D, and 3D using the Sommerfeld free-electron model.
    - express the number and energy of electrons in a system in terms of integrals over k-space.
    - use the Fermi distribution to extend the previous learning goal to finite T.
    - calculate the electron contribution to the specific heat of a solid.
    - describe central terms such as the Fermi energy, Fermi temperature, and Fermi wavevector.


Atoms in a metal provide conduction electrons from their outer shells (often s-shells). These can be described as waves in the crystal, analogous to phonons. Hamiltonian of a free electron:

$$
\mathcal{H}=\frac{ {\bf p}^2}{2m}=-\frac{\hbar^2}{2m}\left( \frac{\partial^2}{\partial x^2}+\frac{\partial^2}{\partial y^2}+\frac{\partial^2}{\partial z^2} \right)\ \Rightarrow\ \varepsilon=\frac{\hbar^2}{2m}\left( k_x^2+k_y^2+k_z^2 \right)
$$

Take periodic boundary conditions: $\psi(x,y,z)=\psi(x+l,y+L,z+L)$:

$$
k_x=\frac{2\pi p}{L},\ k_y=\frac{2\pi q}{L},\ k_z=\frac{2\pi r}{L}\ \Rightarrow\ \varepsilon=\frac{2\pi^2\hbar^2}{mL^2}\left( p^2+q^2+r^2 \right)
$$

![](figures/free_electron.svg)

Comparable to phonons, but: electrons are _fermions_.

- Only 2 (due to spin) allowed per $k$-value
- Fill up from the lowest energy until you run out of electrons

$\rightarrow$ Calculate when you are out of electrons $\rightarrow$ _Fermi energy_.

![](figures/fermi_circle_periodic.svg)

In order to compute the density of states, we need to perform an integration of k-space. Assuming three dimensions and spherical symmetry (the dispersion in the free electron model is isotropic) we find for the total number of states:

$$
N=2\left(\frac{L}{2\pi}\right)^3\int{\rm d}{\bf k}=2 \left(\frac{L}{2\pi}\right)^34\pi\int k^2{\rm d}k=\frac{V}{\pi^2}\int k^2{\rm d}k ,
$$

where the factor 2 represents spin degeneracy. Using $k=\frac{\sqrt{2m\varepsilon}}{\hbar}$ and ${\rm d}k=\frac{1}{\hbar}\sqrt{\frac{m}{2\varepsilon}}{\rm d}\varepsilon$ we can rewrite this as:

$$
N=\frac{V}{\pi^2}\int\frac{2m\varepsilon}{\hbar^3}\sqrt{\frac{m}{2\varepsilon}}{\rm d}\varepsilon=\frac{Vm^{3/2}}{\pi^2\hbar^3}\int\sqrt{2\varepsilon}\ {\rm d}\varepsilon
$$

So we find for the density of states:

$$
g(\varepsilon)=\frac{ {\rm d}N}{ {\rm d}\varepsilon}=\frac{Vm^{3/2}\sqrt{2\varepsilon}}{\pi^2\hbar^3}\propto\sqrt{\varepsilon}
$$

```python
E = np.linspace(0, 2, 500)
fig, ax = pyplot.subplots()

ax.plot(E, np.sqrt(E))

ax.set_ylabel(r"$g(\varepsilon)$")
ax.set_xlabel(r"$\varepsilon$")
draw_classic_axes(ax, xlabeloffset=.2)
```

Similarly,

- For 1D: $g(\varepsilon) = \frac{2 V}{\pi} \frac{ {\rm d}k}{ {\rm d}\varepsilon} \propto 1/\sqrt{\varepsilon}$
- For 2D: $g(\varepsilon) = \frac{k V}{\pi} \frac{ {\rm d}k}{ {\rm d}\varepsilon} \propto \text{constant}$

Total number of electrons:

$$
N=\int_0^{\varepsilon_{\rm F}}g(\varepsilon){\rm d}\varepsilon,
$$

with $\varepsilon_{\rm F}$ the _Fermi energy_ = highest filled energy at $T=0$.

$$
\varepsilon_{\rm F}=\frac{\hbar^2}{2m}\left( 3\pi^2\frac{N}{V} \right)^{2/3}\equiv \frac{\hbar^2 k_{\rm F}^2}{2m},\
k_{\rm F}=\left( 3\pi^2\frac{N}{V} \right)^{1/3}
$$

The quantity $k_{\rm F}=\frac{2\pi}{\lambda_{\rm F}}$ is called the _Fermi wavevector_, where $\lambda_{\rm F}$ is the _Fermi wavelength_, which is typically in the order of the atomic spacing.

For copper, the Fermi energy is ~7 eV. It would take a temperature of $\sim 70 000$K for electrons to gain such energy through a thermal excitation! The _Fermi velocity_ $v_{\rm F}=\frac{\hbar k_{\rm F}}{m}\approx$ 1750 km/s $\rightarrow$ electrons run with a significant fraction of the speed of light, only because lower energy states are already filled by other electrons.

The total number of electrons can be expressed as $N=\frac{2}{3}\varepsilon_{\rm F}g(\varepsilon_{\rm F})$.

![](figures/fermi_level.svg)

The bold line represents all filled states at $T=0$. This is called the _Fermi sea_. Conduction takes place only at the _Fermi surface_: everything below $\varepsilon_{\rm F}-\frac{eV}{2}$ is compensated.

Now: **Finite temperature** $\rightarrow$ probability distribution to occupy certain states.

Fermi-Dirac distribution:

$$
f(\varepsilon,T)=\frac{1}{ {\rm e}^{(\varepsilon-\mu)/k_{\rm B}T}+1}
$$

```python
fig = pyplot.figure()
ax = fig.add_subplot(1,1,1)
xvals = np.linspace(0, 2, 200)
mu = .75
beta = 20
ax.plot(xvals, xvals < mu, ls='dashed', label='$T=0$')
ax.plot(xvals, 1/(np.exp(beta * (xvals-mu)) + 1),
        ls='solid', label='$T>0$')
ax.set_xlabel(r'$\varepsilon$')
ax.set_ylabel(r'$f(\varepsilon, T)$')
ax.set_yticks([0, 1])
ax.set_yticklabels(['$0$', '$1$'])
ax.set_xticks([mu])
ax.set_xticklabels([r'$\mu$'])
ax.set_ylim(-.1, 1.1)
ax.legend()
draw_classic_axes(ax)
pyplot.tight_layout()
```


Chemical potential $\mu=\varepsilon_{\rm F}$ if $T=0$. Typically $\varepsilon_{\rm F}/k_{\rm B}$~70 000 K (~7 eV), whereas room temperature is only 300 K (~30 meV) $\rightarrow$ thermal smearing occurs only very close to Fermi surface.

At finite temperature, the total number of electrons $N$ should be:

$$
N=\int_0^\infty f(\varepsilon,T)g(\varepsilon){\rm d}\varepsilon=\int_0^\infty n(\varepsilon,T){\rm d}\varepsilon
$$

We can use this to calculate the electronic contribution to the heat capacity.

```python
E = np.linspace(0, 2, 500)
fig, ax = pyplot.subplots()
ax.plot(E, np.sqrt(E), linestyle='dashed')
ax.text(1.7, 1.4, r'$g(\varepsilon)\propto \sqrt{\varepsilon}$', ha='center')
ax.fill_between(E, np.sqrt(E) * (E < 1), alpha=.3)

n = np.sqrt(E) / (1 + np.exp(20*(E-1)))
ax.plot(E, n)
ax.fill_between(E, n, alpha=.5)
w = .17
ax.annotate(s='', xy=(1, 1), xytext=(1-w, 1),
            arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
ax.text(1-w/2, 1.1, r'$\sim k_BT$', ha='center')
ax.plot([1-w, 1+w], [1, 0], c='k', linestyle='dashed')
ax.annotate(s='', xy=(1, 0), xytext=(1, 1),
            arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
ax.text(1.2, .7, r'$g(\varepsilon_F)$', ha='center')
ax.set_xticks([1])
ax.set_xticklabels([r'$\varepsilon_F$'])

ax.set_ylabel(r"$g(\varepsilon)$")
ax.set_xlabel(r"$\varepsilon$")
draw_classic_axes(ax, xlabeloffset=.2)
```

Electrons in the top triangle are being excited to the bottom triangle due to temperature increase. Number of excited electrons $\approx\frac{1}{2}g(\varepsilon_{\rm F})k_{\rm B}T=n_{\rm exc}$. Total extra energy $E(T)-E(0)=n_{\rm exc}k_{\rm B}T=\frac{1}{2}g(\varepsilon_{\rm F})k_{\rm B}^2T^2$.

$$
C_{V,e}=\frac{ {\rm d}E}{ {\rm d}T}=g(\varepsilon_{\rm F})k_{\rm B}^2T=\ ...\ =\frac{3}{2}Nk_{\rm B}\frac{T}{T_{\rm F}}\propto T
$$

$T_{\rm F}=\frac{\varepsilon_{\rm F}}{k_{\rm B}}$ is the _Fermi temperature_.

How does $C_{V,e}$ relate to the phonon contribution $C_{V,p}$?

- At room temperature, $C_{V,p}=3Nk_{\rm B}\gg C_{V,e}$
- Near $T=0$, $C_{V,p}\propto T^3$ and $C_{V,e}\propto T$ $\rightarrow$ competition.

New concept: _Fermi surface_ = all points in k-space with $\varepsilon=\varepsilon_{\rm F}$. For free electrons, the Fermi surface is a sphere.

$$
N=2\frac{\frac{4}{3}\pi k_{\rm F}^3}{\left( \frac{2\pi}{L} \right)^3}=\frac{k_{\rm F}^3V}{3\pi^2}\ \Rightarrow\ k_{\rm F}=\left( 3\pi^2\frac{N}{V} \right)^{1/3}
$$

![](figures/transport.svg)

The orange circle represents the Fermi surface at finite current $\rightarrow$ this circle will shift only slightly before the electrons reach terminal velocity $\rightarrow$ all transport takes place near the Fermi surface.

## Useful trick: scaling of $C_V$

Behavior of $C_V$ can be very quickly memorized or understood using the following mnemonic rule

> Particles with energy $E \leq kT$ are thermally excited, and each carries extra energy $kT$.

#### Example 1: electrons

$g(E_F)$ roughly constant ⇒ total energy in the thermal state is $T \times [T\times g(E_F)]$ ⇒ $C_V \propto T$.

#### Example 2: graphene with $E_F=0$ (midterm 2018)

$g(E) \propto E$ ⇒ total energy is $T \times T^2$ ⇒ $C_V \propto T^2$.

#### Example 3: phonons in 3D at low temperatures.

$g(E) \propto E^2$ ⇒ total energy is $T \times T^3$ ⇒ $C_V \propto T^3$.
