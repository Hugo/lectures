# Lecture 5 – Crystal structure and diffraction

_based on chapters 12–14, (up to and including 14.2) of the book_  
Exercises 12.3, 12.4, 13.3, 13.4, 14.2

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Describe any crystal using crystallographic terminology, and interpret this terminology
    - Compute the volume filling fraction given a crystal structure
    - Determine the primitive, conventional, and Wigner-Seitz unit cells of a given lattice
    - Determine the Miller planes of a given lattice

### Crystal classification

- **_Lattice_**
    + periodic pattern of *lattice points*, which all have an identical view
    + lattice points are not necessarily the same as atom positions
    + there can be multiple atoms per lattice point
    + freedom of translation
    + multiple lattices with different point densities possible
- **_Lattice vectors_**
    + from lattice point to lattice point
    + $N$ vectors for $N$ dimensions
    + multiple combinations possible
    + not all combinations provide full coverage
- **_Unit cell_**
    + spanned by lattice vectors
    + has 4 corners in 2D, 8 corners in 3D
    + check if copying unit cell along lattice vectors gives full lattice
- **_Primitive unit cell_**
    + smallest possible $\rightarrow$ no identical points skipped
    + not always most practical choice
- **_Basis_**
    + only now we care about the contents (i.e. atoms)
    + gives element and position of atoms
    + properly count partial atoms $\rightarrow$ choose which belongs to unit cell
    + positions in terms of lattice vectors, *not* Cartesian coordinates!

### Example: graphite

![](figures/graphite_mod.svg)

1. Choose origin (can be atom, not necessary)
2. Find other lattice points that are identical
3. Choose lattice vectors, either primitive (red) or not primitive (blue)
    - lengths of lattice vectors and angle(s) between them fully define the crystal lattice
    - for graphite: $|{\bf a}_1|=|{\bf a}_2|$ = 0.246 nm = 2.46 Å, $\gamma$ = 60$^{\circ}$
4. Specify basis
    - using ${\bf a}_1$ and ${\bf a}_2$: C$(0,0)$, C$\left(\frac{2}{3},\frac{2}{3}\right)$
    - using ${\bf a}_1$ and ${\bf a}_{2}'$: C$(0,0)$, C$\left(0,\frac{1}{3}\right)$, C$\left(\frac{1}{2},\frac{1}{2}\right)$, C$\left(\frac{1}{2},\frac{5}{6}\right)$

An alternative type of unit cell is the _Wigner-Seitz cell_: the collection of all points that are closer to one specific lattice point than to any other lattice point. You form this cell by taking all the perpendicular bisectrices or lines connecting a lattice point to its neighboring lattice points.

### Stacking of atoms
What determines what crystal structure a material adopts? $\rightarrow$ To good approximation, atoms are solid incrompressible spheres that attract each other. How will these organize?

We start with the densest possible packing in 2D:
![](figures/packing.svg)
Will the second layer go on sites A, B or C?

ABCABC stacking $\rightarrow$ _cubic close packed_, also known as _face centered cubic_ (fcc):

![](figures/stacking.svg)

- One atom on the center of each side-plane: 'a dice that always throws 1'
- Conventional unit cell $\neq$ primitive unit cell
- Cyclic ABC $\rightarrow$ all atoms identical $\rightarrow$ 1 atom per primitive unit cell
- Conventional cell: $8\times\frac{1}{8}+6\times\frac{1}{2}=1+3=4$ atoms

Examples of fcc crystals: Al, Cu, Ni, Pb, Au and Ag.

### Filling factor
Filling factor = # of atoms per cell $\times$ volume of 1 atom / volume of cell

$=\frac{4\times\frac{4}{3}\pi R^3}{a^3}=\frac{1}{6}\sqrt{2}\pi\approx 0.74$, where we have used that $\sqrt{2}a=4R$.

Compare this to _body centered cubic_ (bcc), which consists of a cube with atoms on the corners and one atom in the center (Fig. 1.12): filling factor = 0.68. Examples of bcc crystals: Fe, Na, W, Nb.

Question: is 74% the largest possible filling factor? $\rightarrow$ Kepler conjecture (1571 – 1630). Positive proof by Hales _et al._ in 2015!

Crystal structures that are related to fcc:

1. ionic crystals (Fig. 1.13), e.g. NaCl
2. zincblende (Fig. 1.15), e.g. diamond

ABABAB stacking $\rightarrow$ _hexagonally close-packed_ (hcp), e.g. Co, Zn. In this case there is no cubic symmetry (Fig. 1.11).

### Miller planes
We start with a simple cubic lattice:

![](figures/cubic_mod.svg)

$|{\bf a}_1|=|{\bf a}_2|=|{\bf a}_3|\equiv a$ (_lattice constant_)

The plane designated by Miller indices $(u,v,w)$ intersects lattice vector ${\bf a}_1$ at $\frac{|{\bf a}_1|}{u}$, ${\bf a}_2$ at $\frac{|{\bf a}_2|}{v}$ and ${\bf a}_3$ at $\frac{|{\bf a}_3|}{w}$.

![](figures/miller.svg)

Miller index 0 means that the plane is parallel to that axis (intersection at "$\frac{|{\bf a}_3|}{0}=\infty$"). A bar above a Miller index means intersection at a negative coordinate.

If a crystal is symmetric under $90^\circ$ rotations, then $(100)$, $(010)$ and $(001)$ are physically indistinguishable. This is indicated with $\{100\}$. $[100]$ is a vector. In a cubic crystal, $[100]$ is perpendicular to $(100)$ $\rightarrow$ proof in problem set.
