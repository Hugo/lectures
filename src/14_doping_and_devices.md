```python
from matplotlib import pyplot

import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import quad

from common import draw_classic_axes, configure_plotting

configure_plotting()

def sqrt_plus(x):
    return np.sqrt(x * (x >= 0))

# Band structure parameters.
E_V, E_C, E_F = -1.2, 1.8, .4
E_D, E_A = E_C - .7, E_V + .5
m_h, m_e = 1, .5
```

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Compute carrier density and Fermi level position of doped semiconductors
    - Describe the functioning principles of semiconducting devices

## Adding an impurity to semiconductor

* Typical semiconductors are group IV (Si, Ge, GaAs).
* Unfilled shell of group V atom (donor) has 1 extra electron and its nucleus 1 extra proton
* Group III atom (acceptor) lacks 1 electron and 1 nucleus charge

Extra electron (or extra hole) is attracted to the extra charge of the nucleus.

In H the energy levels are:
$$ E_n = - \frac{me^4}{8\pi^2\hbar^3\varepsilon^2_0n^2} = -R_E /n^2= -\frac{13.6\text{eV}}{n^2}$$

Bohr radius (size of the ground state wave function): $4 \pi \varepsilon_0 \hbar^2/m_{\mathrm{e}} e^2$

In a semiconductor $m\to m_{\text{eff}}$, $\epsilon_0 \to \epsilon\epsilon_0$.

An impurity creates a very weakly bound state:
$$E = -\frac{m_e}{m\varepsilon^2} R_E = -0.01 \text{eV (in Ge)}$$
$r = 4$ nm (vs $r = 0.5$ Å in H).

Binding energy smaller than room temperature (0.026 eV).

So a donor adds an extra state at $E_D$ (close to the bottom of the conduction band) and an extra electron.

Likewise an acceptor adds an extra state at $E_A$ (close to the top of the valence band) and an extra hole.

### Density of states with donors and acceptors

```python
E = np.linspace(-3, 3, 1000)
fig, ax = pyplot.subplots()

n_F = 1/(np.exp(2*(E - E_F)) + 1)
g_e = m_e * sqrt_plus(E - E_C)
g_h = m_h * sqrt_plus(E_V - E)
ax.plot(E, g_h, label="$g_e$")
ax.plot(E, g_e, label="$g_h$")

sigma = 0.01
g_D = np.exp(-(E_D - E)**2 / sigma**2)
g_A = .7 * np.exp(-(E_A - E)**2 / sigma**2)
ax.plot(E, g_D, label='$g_D$')
ax.plot(E, g_A, label='$g_A$')
ax.legend()
ax.set_xticks([E_V, E_C, E_A, E_D])
ax.set_xticklabels(['$E_V$', '$E_C$', '$E_A$', '$E_D$'])
draw_classic_axes(ax, xlabeloffset=.2)
```

All donor/acceptor states at the same energy:
$$g_A(E) = N_A \delta(E-E_A),\quad g_D(E) = N_D \delta(E- (E_G - E_D))$$

How large can $N_D/N_A$ be? The distance between donors should be such that the states don't overlap, so the distance must be much larger than 4 nm. Therefore **maximal** concentration of donors before donor band merges with conduction band is $N_D \lesssim (1Å/4\textrm{nm})^3 \sim 10^{-5}\ll N_C$.


## Number of carriers

Charge conservation:
$$n_e - n_h + n_D - n_A = N_D - N_A$$

We already know $n_e$ and $n_h$.

$$n_D = N_D \frac{1}{e^{-(E_D-E_F)/kT} + 1}, n_A = N_A \frac{1}{e^{-(E_A+E_F)/kT} + 1}$$

Simplification:

Most donors are ionized and most acceptors are occupied.

Then

$$n_e - n_h = N_D - N_A, n_e = n_i^2/n_h$$

When $|N_D-N_A| \gg n_i$ the semiconductor is **extrinsic**, so that if $N_D > N_A$ ($n$-doped semiconductor), $n_e \approx N_D -N_A$ and $n_h = n_i^2/(N_D-N_A)$. If $N_D < N_A$ ($p$-doped semiconductor), $n_h \approx N_A -N_D$ and $n_e = n_i^2/(N_A-N_D)$.

We can now easily find the Fermi level:

$$E_F = E_G - kT\log[N_C/(N_D-N_A)], \textrm{ for } N_D > N_A$$
and
$$E_F = kT\log[N_V/(N_A-N_D)], \textrm{ for } N_A > N_D$$

??? question "When is a semiconductor intrinsic, and when it is extrinsic?"
    By definition the semiconductor is intrinsic when $|N_D-N_A| \ll n_i$, so $kT \gtrsim E_G/\log[N_C N_V/(N_D-N_A)^2]$.

## Temperature dependence of the carrier density and Fermi level

It is instructive to consider how $E_F$, $n_e$ and $n_h$ depend on carrier concentrations.

![](figures/E_F_and_carrier_density.svg)

Several noteworthy features:

* At high temperature $n_e = n_h$ and $E_F$ has an upturn (if holes are heavier than electrons)
* Once the temperature is sufficiently low, we expect the electrons to "freeze away" from the conduction band to the donor band, so that the donor band starts playing a role of the new valence band at $kT \ll E_G - E_D$.
* At zero temperature $E_F$ should match the donor band since it has partially occupied states. If there are no acceptors, $E_F$ would be halfway between $E_D$ and $E_G$, and if there was no doping at all it would be at $E_G/2$.

!!! check "Exercise"
    check that you can reproduce all the relevant limits in a calculation.

## Measuring band gaps

### Conduction

The change of $n_i$ is extremely rapid due to the factor $e^{-E_G/kT}$. Therefore the simplest way of determining the band gap is the temperature dependence of conductance

The total current

$$j = -n_e e v_e + n_h e v_h $$

$$ -m_e v_e /\tau_e = -eE;\quad -m_h v_h /\tau_h = eE $$

Combining the two we see that despite electron and hole velocities have opposite signs, they carry electric current in the same direction.

$$ \sigma \equiv \frac{j}{E} = \left(\frac{n_e e^2 \tau_e}{m_e}+\frac{n_h e^2 \tau_h}{m_h}\right) = n_e e \mu_e + n_h e \mu_h.$$

Since $n_e = n_h = n_i \propto e^{-E_G/kT}$, $E_G \approx d \log \sigma / d [kT]^{-1}$.

Additional information can be obtained using Hall effect. However Hall effect is much more complex in semiconductors since only the current in the direction perpendicular to the applied electric field must vanish. This, however only means that the electron current is opposite of the hole current in that direction, not that the electrons and holes move parallel to the applied current.

### Light adsorption

See [previous lecture](.md#light-adsorption)

## Combining semiconductors: $pn$-junction

Main idea: what happens if we bring two differently doped semiconductors together (one of $p$-type, one of $n$-type)?

### Band diagram

Previously we dealt with homogeneous materials, now coordinate (let's call it $x$) starts playing a role. We can represent the properties of inhomogeneous materials using the **band diagram**. The main idea is to plot dependence of various energies ($E_F$, bottom of conduction band, top of the valence band) as a function of position.

So here is our problem for today:

![](figures/band_diagram_question.svg)

> An important remark is in order: before we counted all energies with respect to the top of the valence band. Now $E_F$ is the same everywhere if the sample is in equilibrium, and the bands are shifted due to an extra electrostatic potential.

More specifically: How fast does the electrostatic potential change in the intermediate region? What is the charge density at the junction if we make the chemically-defined boundary between materials very precise?

We may instead use a key bit of insight: **the density of electrons and holes drops exponentially fast as soon as potential deviates by $kT \ll \delta \varphi$ from its bulk value**.

Using this information we can expect a formation of the **depletion region** with almost no electrons or holes (but donors and acceptors won't move anywhere from that region).

So this is our expectation of the dependence of $\rho(x)$:

![](figures/pn_charge_density.svg)

The typical values of $w_n+w_p$ are $\sim 1 \mu \textrm{m}$ at $N_A,\,N_D \sim 10^{16} \textrm{cm}^{-3}$, and $\sim 0.1 \mu \textrm{m}$ at $N_A,\,N_D \sim 10^{18} \textrm{cm}^{-3}$, so it may be much larger than the distance between the dopant atoms.


### $pn$-junction diode

What happens if we apply voltage to a junction?

Because the conductivity of the $p$-region and $n$-region is much larger than that of the depletion region, most of the voltage difference will appear in the depletion region:

![](figures/pn_junction_bias.svg)

The number of majority carriers moving across the junction is proportional to their concentration.
Increasing the voltage bias "pushes" carriers up in energy, it depends exponentially on the voltage.

We therefore get the Shockley diode equation:

$$I = I_0 \left(\exp(eV/kT) -1\right)$$

### Solar cell

Light adsopbed in the $pn$-junction creates electron-hole pairs.
The eletric field then moves electrons to the $n$-doped region, holes to the $p$-doped one, and therefore generates a voltage.

![](figures/solar_cell.svg)

### Semiconducting laser

A heavily doped $pn$-junction so that the Fermi level is in the conduction/valence band produces an extremely high rate electron-hole recombination with an extremely high rate, and makes the $pn$-junction function like a laser.

### MOSFET and quantum well

See the book for details.

## Summary

Density of states in a doped semiconductor:

```python
fig
```

Charge balance determins the number of electrons and holes as well as the position of the Fermi level.

If dopant concentrations are low, then $n_e = n_h = n_i \equiv \sqrt{N_C N_V}e^{-E_G/2kT}$.

If dopant concentrations are high, then in $n$-doped semiconductor $n_e = N_D - N_A$ and $n_h = n_i^2/n_e$ (or vice versa).

Temperature switches between intrinsic and extrinsic regimes, and controls the carrier density

Conductance combines the contributions of electrons and holes, and allows to determine $E_G$.

A $pn$-junction has a **depletion layer** in its middle with the potential in a $pn$-junction having the following shape (where the transition region is made out of two parabolas):

![](figures/band_diagram_solution.svg)
