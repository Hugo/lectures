```python
from matplotlib import pyplot

import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import quad

from common import draw_classic_axes, configure_plotting

configure_plotting()
```

# Lecture 1 – Phonons and specific Heat
_(based on chapter 2 of the book)_  
Exercises: 2.3, 2.4, 2.5, 2.6, 2.8

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Explain quantum mechanical effects on the heat capacity of solids (Einstein model)
    - Compute the expected particle number, energy, and heat capacity of a quantum harmonic oscillator (a single boson)
    - Write down the total thermal energy of a material

### Einstein model
Before solid state physics: heat capacity per atom $C=3k_{\rm B}$ (Dulong-Petit). Each atom is (classical) harmonic oscillator in three directions. Experiments showed that this law breaks down at low temperatures, where $C$ reduces to zero ($C\propto T^3$).

This can be explained by considering a _quantum_ harmonic oscillator:
![](figures/harmonic.svg)

$$\varepsilon_n=\left(n+\frac{1}{2}\right)\hbar\omega$$

Phonons are bosons $\Rightarrow$ they follow Bose-Einstein statistics.

$$
n(\omega,T)=\frac{1}{ {\rm e}^{\hbar\omega/k_{\rm B}T}-1}\Rightarrow\bar{\varepsilon}=\frac{1}{2}\hbar\omega+\frac{\hbar\omega}{ {\rm e}^{\hbar\omega/k_{\rm B}T}-1}
$$

```python
fig, (ax, ax2) = pyplot.subplots(ncols=2, figsize=(10, 5))
omega = np.linspace(0.1, 2)
ax.plot(omega, 1/(np.exp(omega) - 1))
ax.set_ylim(0, top=3)
ax.set_xlim(left=0)
ax.set_xlabel(r'$\hbar \omega$')
ax.set_xticks([0, 1])
ax.set_xticklabels(['$0$', '$k_B T$'])
ax.set_ylabel('$n$')
ax.set_yticks([1, 2])
ax.set_yticklabels(['$1$', '$2$'])
draw_classic_axes(ax, xlabeloffset=.2)
T = np.linspace(0.01, 2)
ax2.plot(T, 1/2 + 1/(np.exp(1/T)-1))
ax2.set_ylim(bottom=0)
ax2.set_xlabel('$k_B T$')
ax2.set_xticks([0, 1])
ax2.set_xticklabels(['$0$', r'$\hbar \omega$'])
ax2.set_ylabel(r"$\bar\varepsilon$")
ax2.set_yticks([1/2])
ax2.set_yticklabels([r'$\hbar\omega/2$'])
draw_classic_axes(ax2, xlabeloffset=.15)
```

The term $\frac{1}{2}\hbar\omega$ is the _zero point energy_, which follows from the uncertainty principle.

In order to calculate the heat capacity per atom $C$, we need to differentiate $\bar{\varepsilon}$ to $T$.

$$
\begin{multline}
C = \frac{\partial\bar{\varepsilon}}{\partial T}
= -\frac{\hbar\omega}{\left({\rm e}^{\hbar\omega/k_{\rm B}T}-1\right)^2}\frac{\partial}{\partial T}\left({\rm e}^{\hbar\omega/k_{\rm B}T}-1\right)\\
= \frac{\hbar^2\omega^2}{k_{\rm B}T^2}\frac{ {\rm e}^{\hbar\omega/k_{\rm B}T}}{\left({\rm e}^{\hbar\omega/k_{\rm B}T}-1\right)^2}
=k_{\rm B}\left(\frac{\hbar\omega}{k_{\rm B}T}\right)^2\frac{ {\rm e}^{\hbar\omega/k_{\rm B}T}}{\left({\rm e}^{\hbar\omega/k_{\rm B}T}-1\right)^2}
\end{multline}
$$

```python
def c_einstein(T, T_E=1):
    x = T_E / T
    return 3 * x**2 * np.exp(x) / (np.exp(x) - 1)**2

T = np.linspace(0.01, 1.5, 500)
fig, ax = pyplot.subplots()

ax.plot(T, c_einstein(T)/3)
ax.fill_between(T, c_einstein(T)/3, 1, alpha=0.5)

ax.set_ylim(bottom=0, top=1.2)
ax.set_xlabel('$T$')
ax.set_ylabel(r'$\omega$')
ax.set_xticks([1])
ax.set_xticklabels([r'$\hbar \omega/k_B$'])
ax.set_yticks([1])
ax.set_yticklabels(['$k_B$'])
pyplot.hlines([1], 0, 1.5, linestyles='dashed')
draw_classic_axes(ax)
```

The dashed line signifies the classical value, $k_{\rm B}$. Shaded area $=\frac{1}{2}\hbar\omega$, the zero point energy that cannot be removed through cooling.

This is for just one atom. In order to obtain the heat capacity of a full material, we would have to multiply $C$ (or $\bar{\varepsilon}$) by $3N$, _i.e._ the number of harmonic oscillators according to Einstein model.

```python
# Data from Einstein's paper
T = [222.4, 262.4, 283.7, 306.4, 331.3, 358.5, 413.0, 479.2, 520.0, 879.7, 1079.7, 1258.0]
c = [0.384, 0.578, 0.683, 0.798, 0.928, 1.069, 1.343, 1.656, 1.833, 2.671, 2.720, 2.781]

fit = curve_fit(c_einstein, T, c, 1000)
T_E = fit[0][0]
delta_T_E = np.sqrt(fit[1][0, 0])

fig, ax = pyplot.subplots()
ax.scatter(T, c)
temps = np.linspace(10, T[-1], 100)
ax.plot(temps, c_einstein(temps, T_E));
ax.set_xlabel('$T[K]$')
ax.set_ylabel('$C/k_B$')
ax.set_ylim((0, 3));
```
